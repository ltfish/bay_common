<?php


namespace Bay\Oss;

use OSS\OssClient;
use OSS\Core\OssException;

/**
 * oss
 * Author JanChan
 * Date 2021-06-23 15:20:00
 * Class UpLoadFile
 * @package Bay\Oss
 */
class UpLoadFile
{

    private $accessKeyId;

    private $accessKeySecret;

    private $endpoint;

    /**
     * 初始化配置
     * UpLoadFile constructor
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->endpoint = $config["endpoint"] ?? env("Oss_endPoint", "https://oss-cn-chengdu.aliyuncs.com");
        $this->accessKeyId = $config["accessId"] ?? env("Oss_accessId");
        $this->accessKeySecret = $config["accessKey"] ?? env("Oss_accessKey");
    }

    /**
     * 字符串模式上传文件
     * @param $bucket
     * @param $fileName
     * @param $content
     * @return bool
     */
    public function upLoadStream($bucket, $fileName, $content): bool
    {
        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
            $ossClient->putObject($bucket, $fileName, $content);
        } catch (OssException $e) {
            return false;
        }
        return true;
    }

    /**
     * 文件路径上传
     * @param $bucket
     * @param $fileName
     * @param $filePath
     * @return bool
     */
    public function upLoadFile($bucket, $fileName, $filePath): bool
    {
        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
            $ossClient->uploadFile($bucket, $fileName, $filePath);
        } catch (OssException $e) {
            return false;
        }
        return true;
    }

}
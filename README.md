# 81号兵站公共composer

环境安装： 必备扩展(openssl/redis/pdo/mongodb/memcached/msgpack)

使用方法：

# 1、第一步在composer文件中引入该包

```json
{
  "require": {
    "bay/common": "dev-master"
  },
  "repositories": {
    "0": {
      "type": "git",
      "secure-http": false,
      "url": "https://gitee.com/ltfish/bay_common.git",
      "reference": "dev"
    }
  }
}


```

# 配置

```env
#发送消息请求地址
MNS_endPoint=
#appid
MNS_accessId=
#appkey
MNS_accessKey=
#oss文件上传
Oss_endPoint=
Oss_accessId=
Oss_accessKey=
```

# MNS使用

```php
<?php


namespace App\Http\Controllers;


use Bay\Mns\MnsHelper;
use Bay\Oss\UpLoadFile;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {

        $mns = new MnsHelper();
        $topic = "city";
        $message = [];
        $message["type"] = "coupon";
        $message["coupon_id"] = 2;
        $res = $mns->topic($topic, json_encode($message));
        return $this->success($res);
    }
    /**
     * 文件上传
     * @throws \Exception
    */
    public function upLoad(){
         $oss=new UpLoadFile();
         $filePath="/temp/1.jpg";
         $fileName=time().random_bytes(4)."jpg";
         $oss->upLoadFile("city-app-v1",$fileName,$filePath);
    }


}


```

# 嵌套demo

```php 

<?php

namespace App\Http\Controllers\Api;


use Bay\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class TestController
 * @package App\Http\Controllers\Api
 */
class TestController extends Controller
{
    /**
     * 嵌套查询-demo 查询所有分类和该分类下消息列表
     * 
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function test(Request $request)
    {
        $res = Db::table("businesier_message");
        $message_type = DB::table("message_type");
        $types = $message_type->get()->toArray();
        $typeIds = array_column($types, "id");
        $some = $res->whereIn("type", $typeIds)->get()->toArray();
        $r = CommonHelper::arrayChangeMultiKey($some, "type");
        foreach ($types as &$v) {
            $v->messages = $r[$v->id] ?? [];
        }
        return $this->success($types);
    }
}


```
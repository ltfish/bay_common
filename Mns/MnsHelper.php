<?php

namespace Bay\Mns;

use AliyunMNS\Client;
use AliyunMNS\Exception\MnsException;
use AliyunMNS\Requests\PublishMessageRequest;

/**
 * MNS微消息服务封装
 * author JanChan
 * 2021-05-08
 * Class MnsHelper
 * @package Mns
 */
class MnsHelper
{

    /**
     * @var mixed
     * 消息发送地址
     */
    private $endPoint;
    /**
     * @var mixed
     * appId
     */
    private $accessId;
    /**
     * @var mixed
     * secretKey
     */
    private $accessKey;

    /**
     * 初始化MNS配置
     * MnsHelper constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->endPoint = $config["endPoint"] ?? env("MNS_endPoint");
        $this->accessId = $config["accessId"] ?? env("MNS_accessId");
        $this->accessKey = $config["accessKey"] ?? env("MNS_accessKey");
    }

    /**
     * @param string $topic
     * @param string $messageBody
     * @return \AliyunMNS\Responses\BaseResponse|false
     * 主题模式发送消息->订阅终端
     */
    public function topic(string $topic = "city", string $messageBody = "test")
    {
        $client = new Client($this->endPoint, $this->accessId, $this->accessKey);
        $queue = $client->getTopicRef($topic);
        $data = new PublishMessageRequest($messageBody);
        try {
            return $queue->publishMessage($data);
        } catch (MnsException $e) {
            return false;
        }
    }

}
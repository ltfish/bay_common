<?php


namespace Bay\Helper;

/**
 * 常用函数
 *
 * Class CommonHelper
 * @package Bay\Helper
 * @Author JanChan
 */
class CommonHelper
{

    /**
     * 数组换键 key的值必须唯一(1对1)
     * @param array $arr
     * @param string $key
     * @return array
     */
    public static function arrayChangePriKey(array $arr, string $key): array
    {
        $returnArray = [];
        if (sizeof($arr) == 0)
            return $arr;
        foreach ($arr as $v) {
            if (is_object($v)) {
                if (isset($v->$key)) {
                    $returnArray[$v->$key] = $v;
                }
            } else {
                if (isset($v[$key])) {
                    $returnArray[$v[$key]] = $v;
                }
            }
        }
        return $returnArray;

    }

    /**
     * 数组换键 1对多
     * @param array $arr
     * @param string $key
     * @return array
     */
    public static function arrayChangeMultiKey(array $arr, string $key): array
    {
        $returnArray = [];
        if (sizeof($arr) == 0)
            return $arr;
        foreach ($arr as $v) {
            if (is_object($v)) {
                if (isset($v->$key)) {
                    $temp = $v->$key;
                    if (isset($returnArray[$temp])) {
                        array_push($returnArray[$temp], $v);
                    } else {
                        $returnArray[$temp] = [$v];
                    }

                }
            } else {
                if (isset($v[$key])) {
                    $temp = $v[$key];
                    if (isset($returnArray[$temp])) {
                        array_push($returnArray[$temp], $v);
                    } else {
                        $returnArray[$temp] = [$v];
                    }

                }
            }
        }
        return $returnArray;

    }

    /**
     * 多维 2，3，4 多对多
     *
     * @param $arr
     * @param $key
     * @return mixed
     */
    public static function arrayChangeManyKey(array $arr, string $key): array
    {
        $returnArray = [];
        if (sizeof($arr) == 0)
            return $arr;
        foreach ($arr as $item) {
            if (is_object($item)) {
                $item = (array)$item;
            };
            if (isset($item[$key])) {
                $keys = explode(",", $item[$key]);
                foreach ($keys as $k) {
                    if (isset($returnArray[$k])) {
                        array_push($returnArray[$k], $item);
                    } else {
                        $returnArray[$k] = [$item];
                    }
                }
            }

        }
        return $returnArray;

    }
}